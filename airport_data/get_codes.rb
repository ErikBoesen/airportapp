require 'json'

#downloaded stations from http://api.weather.gov/stations (deleted for space)
stations = JSON.parse(File.read('stations'))

f = File.open('stations.csv', 'w')

stations['features'].each do |stn|
  props = stn['properties']
  f.write "#{props['stationIdentifier']},#{props['name']},#{props['timeZone']}\n"
end

f.close
