class AirportsController < ApplicationController
    
    def index
      @airports = Airport.all
      @airports.each do |ap|
        ap.load_stn
        ap.load_weather
      end
    end

    def show
    end

    def new
        @airport = Airport.new
    end
     
    def create
        @airport = Airport.new(airport_params)

        if @airport.save
          redirect_to root_url, notice: 'Airport was successfully created.'
        else
          render :new
        end
    end
      
    
    def destroy
      @airport = Airport.find(params[:id])
      @airport.destroy
      redirect_to airports_url, notice: 'Airport was successfully deleted.'
    end
    

    private
    def airport_params
      params.require(:airport).permit(:name, :code)
    end
      
end
