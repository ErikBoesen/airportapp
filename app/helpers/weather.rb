require 'net/http'
require 'json'
require 'tzinfo'

class Station
  
  attr_accessor :name, :timezone, :descrip, :temp, :humidity, :wind_speed, :wind_dir, :local_time
  
  def initialize(id)
    @id = id
    load_stn
    load_weather
  end

  def load_stn
    resp = Net::HTTP.get_response(URI("https://api.weather.gov/stations/#{@id}"))
    if resp.code != "200"
      return nil
    end
    
    data = JSON.parse(resp.body)
    props = data['properties']
    @name = props['name']
    @timezone = TZInfo::Timezone.get(props['timeZone'])
  end
  
  def load_weather
    resp = Net::HTTP.get_response(URI("https://api.weather.gov/stations/#{@id}/observations/latest"))
    if resp.code != "200"
      return nil
    end
    
    data = JSON.parse(resp.body)
    props = data['properties']
    @descrip = props['textDescription']
    @temp = parseUnits(props['temperature'])
    @humidity = parseUnits(props['relativeHumidity'])
    @wind_speed = parseUnits(props['windSpeed'])
    @wind_dir = parseUnits(props['windDirection'])

    @local_time = @timezone.utc_to_local(Time.now.utc)
  end

  def parseUnits(value)
    units = value['unitCode'].split(":")[1]
    return "%0.2f %s" % [value['value'], units]
  end

  def self.is_valid?(id)
    resp = Net::HTTP.get_response(URI("https://api.weather.gov/stations"))
    puts resp
    if resp.code != "200"
      return nil
    end
    data = JSON.parse(resp.body)
    ids = data['features'].map {|f| f['properties']['stationIdentifier']}
    return ids.include?(id)
  end
end
