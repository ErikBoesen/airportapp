class Airport < ApplicationRecord
  validates :code, presence:true, uniqueness: true
  validates :code, :length => {:maximum => 4,
                               :too_long => "Code should be 3 letters (IATA) or 4 letters (weather station)"}
  validate :check_code
  
  attr_accessor :name, :timezone, :descrip, :temp, :humidity, :wind_speed, :wind_dir, :local_time

  def load_stn
    resp = Net::HTTP.get_response(URI("https://api.weather.gov/stations/#{Airport.iata_to_stn(code)}"))
    if resp.code != "200"
      return nil
    end
    
    data = JSON.parse(resp.body)
    @status = data['status']
    if data['status'] == 404
      return nil
    end
    
    props = data['properties']
    @name = props['name']
    @timezone = TZInfo::Timezone.get(props['timeZone'])
  end

  def load_weather
    puts Airport.iata_to_stn(code)
    resp = Net::HTTP.get_response(URI("https://api.weather.gov/stations/#{Airport.iata_to_stn(code)}/observations/latest"))
    if resp.code != "200"
      return nil
    end
    
    data = JSON.parse(resp.body)

    if data['status'] == "404"
      return nil
    end
    props = data['properties']
    @descrip = props['textDescription']
    @temp = parseUnits(props['temperature'])
    @humidity = parseUnits(props['relativeHumidity'])
    @wind_speed = parseUnits(props['windSpeed'])
    @wind_dir = parseUnits(props['windDirection'])

    @local_time = @timezone.utc_to_local(Time.now.utc)
  end

  def parseUnits(value)
    units = value['unitCode'].split(":")[1]
    if value['value']
      return "%0.2f %s" % [value['value'], units]
    else
      return "-"
    end
  end


  #doing our best to make a valid code out of the input
  def self.iata_to_stn(code)
    if code.length == 3
      return "K" + code.upcase
    end
    return code.upcase
  end    

  def check_code
    resp = Net::HTTP.get_response(URI("https://api.weather.gov/stations"))
    if resp.code != "200"
      errors.add(:code, "couldn't be validate (API call failed)--please retry")
    else
      data = JSON.parse(resp.body)
      ids = data['features'].map {|f| f['properties']['stationIdentifier']}
      if !ids.include?(Airport.iata_to_stn(code))
        errors.add(:code, "is invalid. please enter a valid IATA airport code")
      end
    end
  end

end
