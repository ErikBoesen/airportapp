require "rails_helper"

RSpec.feature "User adds an invalid airport" do
  scenario "they see an error if airport code is blank" do
    visit root_path
    click_on "New Airport"
    click_on "Create Airport"

    expect(page).to have_content("can't be blank")
  end
end
