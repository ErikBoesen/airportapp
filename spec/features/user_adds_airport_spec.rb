require "rails_helper"

RSpec.feature "User adds a (valid)  airport" do
  scenario "they see the right data for that airport" do
    visit root_path
    click_on "New Airport"
    fill_in "airport_code", with: "KEWR"
    click_on "Create Airport"

    expect(page).to have_content("Newark")
  end
end
    
